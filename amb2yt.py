#! /usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

"""
Created on Sat May 7 10:27:44 2016

@author: heller
"""
# =============================================================================
# This file is part of the Ambisonic Decoder Toolbox (ADT).
# Copyright (C) 2016  Aaron J. Heller
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# =============================================================================

import os
import sys
import shutil
import tempfile
import contextlib

# get spatialmedia module from https://github.com/google/spatial-media.git
#  install in same directory as this script

sys.path.append(os.path.dirname(__file__))

from spatialmedia import metadata_utils as mu


@contextlib.contextmanager
def make_temp_directory(working_dir='.'):
    "context manager to create and delete a temporary directory"
    temp_dir = tempfile.mkdtemp(dir=working_dir)
    yield temp_dir
    shutil.rmtree(temp_dir)


def convert_and_inject(af_path, vf_path=None, out_path=None,
                       normalize_dBFS=-1):
    "convert audio and video to appropriate formats, mux, and inject spatial media metadata"

    if vf_path is None:
        vf_path = os.path.join(os.path.dirname(__file__), "cube.jpg")

    vf_file, vf_ext = os.path.splitext(vf_path)
    if vf_ext[1:].lower() in ['jpg', 'jpeg', 'png', 'tif', 'tiff']:
        vf_loop = "-loop 1"
        stop_criteria = "-shortest"
    else:
        vf_loop = "-loop 0"
        stop_criteria = "-longest"

    if out_path is None:
        out_path = os.path.splitext(af_path)[0] + '.mov'

    if normalize_dBFS is None:  # normalize_dBFS = 0 is interpreted as false
        norm_effect = ""
    else:
        norm_effect = "norm %d" % float(normalize_dBFS)

    with make_temp_directory() as temp_dir:
        temp_ambix = os.path.join(temp_dir, 'temp_ambix.wav')
        temp_mov = os.path.join(temp_dir, 'temp_video.mov')

        # convert first-order FuMa to ACN/SN3D (ambiX) and normalize
        os.system(("sox --multi-threaded " +
                   "{af_path} " +
                   "-t wavpcm {temp_ambix} " +
                   "remix -m 1 3v0.7071 4v0.7071 2v0.7071 " +
                   "{norm_effect} stats")
                  .format(**locals()))

        # encode video and mux with audio

        # clues: https://trac.ffmpeg.org/wiki/Encode/AAC
        audio_codec = "libfdk_aac -b:a 512k"  # AAC-LC @ 512 kb/s CBR
        #audio_codec = "pcm_s16le"            # PCM signed 16-bit

        # clues: https://trac.ffmpeg.org/wiki/Limiting%20the%20output%20bitrate
        video_codec = "libx264 -b:v 40000k -bufsize 40000k"

        os.system(
            ("ffmpeg -loglevel info -threads 16 " +
             "{vf_loop} -i {vf_path} " +
             "-guess_layout_max 0 -i {temp_ambix} " +
             "-map 1:a -map 0:v " +
             "-c:a {audio_codec} -channel_layout 4.0 " +
             "-c:v {video_codec} -pix_fmt yuv420p " +
             "{stop_criteria} {temp_mov}").format(**locals()))

        # inject YouTube spatial metadata for spherical video and ambi audio
        metadata = mu.Metadata()
        metadata.audio = mu.SPATIAL_AUDIO_DEFAULT_METADATA
        metadata.video = mu.generate_spherical_xml()

        mu.parse_metadata(temp_mov, print)
        mu.inject_metadata(temp_mov, out_path, metadata, print)
        mu.parse_metadata(out_path, print)

    return out_path


def usage(*argv):
    "provide clues"
    print(os.path.basename(sys.argv[0]) +
          ' usage: audio_file [video_file [out_file [dBFS_normalization]]]')


if __name__ == "__main__":
    if len(sys.argv) > 1:
        out_path = convert_and_inject(*sys.argv[1:])
        print("Done!  Finished file is: " + out_path)

    else:
        usage(sys.argv)
