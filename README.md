# README #

This is a simple python script that converts First-order Ambisonic format files (.amb) to YouTube's format.

### Dependancies ###

* Sox - [http://sox.sourceforge.net](http://sox.sourceforge.net)
* ffmpeg - [https://ffmpeg.org](https://ffmpeg.org)
* YouTube's Spatial Media Metadata Injector module [https://github.com/google/spatial-media.git](https://github.com/google/spatial-media.git)

### Usage ###

    python amb2yt.py audio_file [video_file [out_file [dBFS_normalization]]]

* if no video file is specified, it will use the included cube file
* Normalization defaults to -1 dBFS

### Workflow implemented ###

* Use Sox to convert from AMB/FuMa to ambiX channel order and normalization in a PCM_WAV file

          sox foa.amb -t wavpcm tmp-ambix.wav remix -m 1 3v0.7071 4v0.7071 2v0.7071 norm -1.0 stats
	 
* Use ffmpeg to mux the audio and video into an .MOV file with appropriate codecs

          ffmpeg  -loglevel info -threads 16 -loop 0 \
            -i video.mov -guess_layout_max 0 \
            -i tmp-ambix.wav \
            -map 1:a -map 0:v \
            -c:a libfdk_aac -b:a 512k -channel_layout 4.0 \
            -c:v libx264 -b:v 40000k -bufsize 40000k -pix_fmt yuv420p \
            -longest \
            temp-video.mov
	   
* use YouTube spatial media injector

### Example files produced by this code ###

* AJH eight positions 20x AAC-384k [https://youtu.be/eY9DMn8pgGA]
* Dvořák: Carnival Overture, Op92 [https://youtu.be/mDVVyNKkhSA]
* MC 26 positions 4x [https://youtu.be/RC4ptd9B-NA]


### Author ###

* Aaron Heller <heller@ai.sri.com>